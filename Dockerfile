FROM python:3.9.8-bullseye

RUN useradd -d /opt/appuser -s /bin/bash appuser && \
  pip3 install flask pymongo dnspython
ADD --chown=appuser:appuser app.py /opt/appuser/

USER appuser
EXPOSE 8080

ENTRYPOINT ["python3", "/opt/appuser/app.py"]
