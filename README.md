# Docker Exercise 

1. Build the app image
```
docker build -t app .
```

2. Run the compose file
```
docker-compose up
```

3. Make a test request
```
curl --request POST --data foo http://localhost:8080/post
```
