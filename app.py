#!/usr/bin/env python3

from datetime import datetime
from flask import Flask, request
import pymongo
import os

app = Flask(__name__)

@app.route('/')
def mainRoute():
    return "Hello World!"

@app.route('/post', methods = ['POST'])
def postRoute():
    postTimestamp = datetime.now()
    postData = request.get_data(as_text=True)
    
    data = postData + "bar"
    documentData = {"timestamp": postTimestamp, "original_string": postData, "string": data}

    # mongodb
    mongoConnString = os.environ['APP_MONGODB_CONN_STRING']
    mongoClient = pymongo.MongoClient(mongoConnString)
    mongoDatabase = mongoClient["strings"]
    mongoCollection = mongoDatabase["post_requests"]
    mongoInsert = mongoCollection.insert_one(documentData)

    result = "Document inserted with id: " + str(mongoInsert.inserted_id)    
    return result

if __name__ == "__main__":
    app.run(debug=True, host=os.environ['APP_HOST'], port=8080)
